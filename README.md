Fedansy - a set of useful Ansible tasks for Fedora users
========================================================

Introduction
------------

This project is an attempt to provide an easy way to install and/or
activate software that isn't yet or can't become part of the
[Fedora project](https://start.fedoraproject.org/ "Start with Fedora"),
either because it is proprietary or for any other reason, like an unclear
license.

The idea is heavily taken from [Fedy](http://folkswithhats.org/
"Folks with hats"), which is a very fine project and probably for the
foreseeable future much easier to use than the present project.

I've analysed the Fedy code and couldn't find anything wrong with it, and
many good ideas, but there are some persons who refuse any support
regarding Fedora, in case they detect that you're using Fedy.

For this reason, and in order to train my Ansible skills, I've started this
small project. I hope it'll help the one or the other.

If I find the time and the need to learn some Python-GUI-stuff, it might even
get at some point in time a GUI...


Installation
------------

1. Install Git and Ansible: `sudo dnf install git ansible`
1. Go to a "safe" place and clone this project:

        git clone https://gitlab.com/L1/fedansy.git

Usage
-----

1. Go into the newly created `fedansy` repository.
1. The current user should have sudo access or you'll need to work as root.
1. Check the beginning of `fedansy.yml` until I've found more time to
   document properly this project and/or add a simpler wrapper script.
